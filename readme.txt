Complete the table below with your results, and then provide your interpretation at the end.

Note that:

- When calculating the parallel speed-up S, use the time output by the code, which corresponds
  to the parallel calculation and does not include reading in the file or performing the serial check.

- Take as the serial execution time the time output by the code when run with a single process
  (hence the speed-up for 1 process must be 1.0, as already filled in the table).


No. Process:                        Mean time (average of 3 runs)           Parallel speed-up, S:
===========                         ============================:           ====================
1                                      0.000847316                                   1.0
2                                      0.000870546                                   0.9733155973377627
4                                      0.001026391                                   0.8255294522262958

Architecture that the timing runs were performed on:
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                64
On-line CPU(s) list:   0-63
Thread(s) per core:    2
Core(s) per socket:    16
Socket(s):             2


A brief interpretation of these results (2-3 sentences should be enough):
As seen from the above table the more cores used the longer it takes to calculate,
this can be explained by one main reason that being the overhead of using
parallel methods of computation due to the time it takes to divided and reconstitute
the data from one to multiple processes and back again. This can be shown not just
through the increase of time on any amount greater than 1 process but also as you go from
2 to 4 it still takes longer as once again more pre-processing needs to be done to prepare the data.
