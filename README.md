# ParallelComp_CW2
- Use collective communication to distribute data and accumulate a result.
- Implement a binary tree using point-to-point communication.
- Perform timing runs for parallel scaling and interpret your findings.
